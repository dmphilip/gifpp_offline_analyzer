# offline_analyzer

analyzer for GIFpp data obtained from MIDAS DAQ

for irradiation data:

./offline_analyzer --verbose 1 --runtype MONITORING --runrange 5000 5020 --root

for data during annealing:

./offline_analyzer --verbose 1  --runtype MIP --runrange 5000 5020 --root --annealing

all available keys:

--runtype RUNTYPE - now working runtypes are BVSCAN, LED, MONITORING, MIP, ALL;

--runlist /home/user/blabla/... - use custom runlist file;

--runnumber 99999 - for specific run (note: runnumber should be in runlist);

--runrange 6666 99999 - for all runs in range obtained from runlist;

--stdout - if you want to put output data in standart output of terminal;

--verbose LEVEL - if you want to print more info in terminal;

--root - if you want to dump all data also in root TTree;

--pathname - where all runs are living;

--annealing - special key for annealing;

--before - special key to analyze data from pre-experiment runs;